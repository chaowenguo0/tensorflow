import tensorflow, pandas, builtins, math
tpu = tensorflow.distribute.cluster_resolver.TPUClusterResolver()
tensorflow.config.experimental_connect_to_cluster(tpu)
tensorflow.tpu.experimental.initialize_tpu_system(tpu)
strategy = tensorflow.distribute.TPUStrategy(tpu)
xTrain=pandas.read_csv('../input/mnist-in-csv/mnist_train.csv').drop('label', axis=1).to_numpy()
length = builtins.int(math.sqrt(xTrain.shape[1]))
yTrain=pandas.read_csv('../input/mnist-in-csv/mnist_train.csv')['label'].to_numpy()
xTest=pandas.read_csv('../input/mnist-in-csv/mnist_test.csv').drop('label', axis=1).to_numpy()
yTest=pandas.read_csv('../input/mnist-in-csv/mnist_test.csv')['label'].to_numpy()
xTrain=xTrain.reshape(xTrain.shape[0], length, length, 1)
xTest=xTest.reshape(xTest.shape[0], length, length, 1)
xTrain, xTest = xTrain / 255.0, xTest / 255.0
with strategy.scope():
    model = tensorflow.keras.models.Sequential([
        tensorflow.keras.layers.Conv2D(32, (3, 3), activation='relu', input_shape=(length, length, 1)),
        tensorflow.keras.layers.MaxPooling2D((2, 2)),
        tensorflow.keras.layers.Conv2D(64, (3, 3), activation='relu'),
        tensorflow.keras.layers.MaxPooling2D((2, 2)),
        tensorflow.keras.layers.Conv2D(64, (3, 3), activation='relu'),
        tensorflow.keras.layers.Flatten(),
        tensorflow.keras.layers.Dense(64, activation='relu'),
        tensorflow.keras.layers.Dense(10),
    ])
    model.compile(optimizer=tensorflow.keras.optimizers.Adam(), loss=tensorflow.keras.losses.SparseCategoricalCrossentropy(from_logits=True), metrics=['accuracy'])
model.fit(xTrain, yTrain, epochs=5)
model.save('my_model.h5')
model = tensorflow.keras.models.load_model('my_model.h5')
model.evaluate(xTest,  yTest, verbose=2)
probability = tensorflow.keras.Sequential([model, tensorflow.keras.layers.Softmax()])

import numpy
numpy.argmax(probability.predict(xTest[:10]), axis=1) - yTest[:10]